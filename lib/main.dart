import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('These are my Golf Clubs', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('It aint much, but they work', style: TextStyle(
                  color: Colors.blue[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.golf_course, color: Colors.green[500],),
          Text('18')
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.red, Icons.fastfood, 'Food'),
          _buildButtonColumn(Colors.red, Icons.weekend, 'Chair'),
          _buildButtonColumn(Colors.red, Icons.tv, 'TV'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
          'I have been golfing pretty regualarly'
          'for over two years now. Ever since I got a job at a country club, '
          'I have had ample time and opprotunity to practice and play. '
          'I play with my best friend, Enrique, and my dad a lot. '
          'It is a great way to get outside and enjoy the southern California weather. '
          'I was also watching the U.S. Open during this picture.',
          softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/Clubs.jpg',
                width: 600,
                height: 360,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        )
      ],
    );
  }
}
